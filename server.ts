// Check environment
const env_type = process.env.NODE_ENV || 'dev';

// Get dependencies
import express = require('express');
import path = require('path');
import http = require('http');
import bodyParser = require('body-parser');

// Get our API routes
const app = express();
import { api } from './server/routes/api';

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// API Routes
app.use('/api', api);

// Catch all other routes and return the index file (Angular)
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);
server.listen(port, () => console.log(`API running on localhost:${port}`));
