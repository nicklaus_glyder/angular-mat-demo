import { States } from './states';
import { EventEmitter } from 'events';

interface IAddress {
  street: string,
  city: string,
  state: string,
  zip: number;
}

export class Address implements IAddress {
  constructor (
    public street: string,
    public city: string,
    public state: string,
    public zip: number
  ) {
    if (!States[this.state]) {
      EventEmitter.call("inv-state", this.state);
    }
  }
}
