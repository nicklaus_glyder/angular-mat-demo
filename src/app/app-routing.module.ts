import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
  { path: '', redirectTo: '/home(leftnav:about//rightnav:settings)', pathMatch: 'prefix' },
  { path: 'home', component: HomeComponent },
  // Sidenav routes
  { path: 'about', component: AboutComponent, outlet: 'leftnav'},
  { path: 'settings', component: SettingsComponent, outlet: 'rightnav'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
