import { Component, OnInit } from '@angular/core';
import { Address } from '../models/address';
import { States } from '../models/states';

@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.scss']
})
export class AddressFormComponent implements OnInit {
  address = new Address("103 Cross Creek Court", "Central", "SC", 29630);
  states = Object.keys(States);

  constructor() { }

  ngOnInit() {
  }

}
