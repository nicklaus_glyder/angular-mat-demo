import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.scss']
})
export class InputsComponent implements OnInit {
  numResults: number[];
  results: number;
  display: number;

  constructor() { }

  ngOnInit() {
    this.numResults = [15, 30, 45];
    this.results = this.numResults[0];
    this.display = this.results;
  }

  // This is totally not needed since I'm using ngModel, just showing
  // how to bind to component callbacks
  public radioChange(event) {
    this.display = event.value;
  }

}
