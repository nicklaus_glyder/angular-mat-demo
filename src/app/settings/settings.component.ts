import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  settings: {
    label:string,
    value:boolean
  }[]

  sliders: {
    label:string,
    value:number
  }[]

  sliders_v: {
    label:string,
    value:number
  }[]

  constructor() { }

  ngOnInit() {
    this.settings = [
      {label:"Content Servers", value:false},
      {label:"AWS Cloud", value:true},
      {label:"Data Logging", value:false},
      {label:"Email Updates", value:true},
      {label:"Floor is Lava", value:true}
    ];

    this.sliders = [
      {label:"Power Level", value: 44},
      {label:"Thrusters", value: 84},
      {label:"Funkiness", value: 32},
      {label:"Slow Jams", value: 57}
    ]

    this.sliders_v = [
      {label:"Power Level", value: 44},
      {label:"Thrusters", value: 84},
      {label:"Funkiness", value: 32},
      {label:"Slow Jams", value: 57}
    ]
  }

}
